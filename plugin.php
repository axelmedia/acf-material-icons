<?php

/*
Plugin Name: Advanced Custom Fields: Material Icons
Plugin URI: https://bitbucket.org/axelmedia/acf-material-icons
Description: Material Icons selector field
Version: 0.1.0
Author: AxelMedia
Author URI: https://axel-media.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_plugin_material_icons') ) :
    class acf_plugin_material_icons {
        
        // vars
        var $settings;
        
        
        /*
        *  __construct
        *
        *  This function will setup the class functionality
        *
        *  @type	function
        *  @date	17/02/2016
        *  @since	1.0.0
        *
        *  @param	n/a
        *  @return	n/a
        */
        
        function __construct() {
            
            // settings
            // - these will be passed into the field class.
            $this->settings = array(
                'version'	=> '0.1.0',
                'url'		=> plugin_dir_url( __FILE__ ),
                'path'		=> plugin_dir_path( __FILE__ )
            );
            
            
            // set text domain
            // https://codex.wordpress.org/Function_Reference/load_plugin_textdomain
            load_plugin_textdomain( 'acf-material-icons', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' ); 

            // include field
            add_action('acf/include_field_types', 	array($this, 'include_field_types')); // v5
            add_action('acf/register_fields', 		array($this, 'include_field_types')); // v4
            
        }
        
        
        /*
        *  include_field_types
        *
        *  This function will include the field type class
        *
        *  @type	function
        *  @date	17/02/2016
        *  @since	1.0.0
        *
        *  @param	$version (int) major ACF version. Defaults to false
        *  @return	n/a
        */
        
        function include_field_types( $version = false ) {
            
            // support empty $version
            if( !$version ) $version = 4;

            // include
            include_once('fields/helpers.php');
            include_once('fields/v' . $version . '.php');
            
        }
        
    }
    // initialize
    new acf_plugin_material_icons();
    // class_exists check
endif;
